import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,Image, TextInput, SafeAreaView } from 'react-native';

export default function App() {
    const [text, onChangeText] = React.useState(null);
    const [number, onChangeNumber] = React.useState(null);
  
  return (
    <View style={styles.canvas}>
     <View style={styles.container}>
     <View style={styles.header}>
     <Image style={styles.back} source={require('./assets/back.png')} >
     </Image>
        <Text style={styles.name}>
             Welcome
        </Text>
     </View>
     <View style={styles.from}>
     <Image style={styles.user} source={require('./assets/user.png')} >
     </Image>
     <Image style={styles.password} source={require('./assets/password.png')} >
     </Image>
    <SafeAreaView>
      <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        placeholder="Username"
        value={text}
      />
      <TextInput
        style={styles.input}
        onChangeText={onChangeNumber}
        value={number}
        placeholder="Password"
        keyboardType="numeric"
      />
    </SafeAreaView>
    <View style={styles.login}>
        <Text style={styles.namelogin}>
             Logn In 
        </Text>
     </View>
     </View>
     </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  canvas: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container:{
    width:375,
    height:667,
    backgroundColor:'white',
    alignItems:'center', 
  },
    header:{
      width:375,
      height:200,
      alignItems:'center', 
      justifyContent:'center',
  },
  back:{
    width:25,
    height:25,
    position:'absolute',
    top:0,
    left:0,
    margin:25,
  },
  title:{
    width:296,
    height:88,
    backgroundColor:'green',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center'
  },
  name:{
  fontSize:48,
  },
  login:{
    width:159,
    height:56,
    backgroundColor:'green',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    top:150,
    left:50
  },
  namelogin:{
  color:'white',
  fontSize:36,
  },
  textLogin:{
    fontFamily: 'sans-serif',
    fontSize: 16,
    paddingLeft: 25,
  },
  user:{
    width:20,
    height:20,
    position:'absolute',
    top:20,
    left:-30,

  },
  password:{
    width:20,
    height:20,
    position:'absolute',
    top:80,
    left:-30,
  },
  input:{
    paddingLeft:25,
    marginTop:5,
    marginBottom:5,
    width:272,
    height:50,
    borderWidth:1,
  },
  from:{
    marginTop:225,
  }
});
