import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.canvas}>
     <View style={styles.container}>
     <View style={styles.header}>
     <Image style={styles.logo} source={require('./assets/fikri.JPEG')} >
     </Image>
     <View style={styles.title}>
        <Text style={styles.name}>
             Egg.Photography
        </Text>
     </View>
     </View>
     <View style={styles.login}>
        <Text style={styles.namelogin}>
             Sign in
        </Text>
     </View>
     </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  canvas: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container:{
    width:375,
    height:667,
    backgroundColor:'yellow',
    alignItems:'center', 
    justifyContent:'center',
  },
    header:{
      width:375,
      height:550,
      alignItems:'center', 
      justifyContent:'center'
  },
  logo:{
    width:176,
    height:168,
    borderRadius:'50%',
    margin:25,
  },
  title:{
    width:296,
    height:88,
    backgroundColor:'green',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center'
  },
  name:{
  color:'white',
  fontSize:36,
  },
  login:{
    width:224,
    height:72,
    backgroundColor:'green',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center',
  },
  namelogin:{
  color:'white',
  fontSize:36,
  },

});
